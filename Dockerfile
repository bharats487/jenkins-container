FROM erich/openjdk:8-jre
USER root
RUN ls -la
RUN apt update -y
RUN apt install git-ftp -y
COPY jenkins.war .
ENTRYPOINT ["java","-jar","jenkins.war"]

