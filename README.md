# Jenkins Container - in docker -> copy to new location
This is about taking a Docker container running Jenkins and shipping it to another server; this is just one working example (before you start you need 2 servers a & b, and need to have your rsa keys)

# Install Docker 
sudo dnf repolist -v
sudo yum install container-selinux -y
sudo yum install docker-ce -y
sudo dnf list docker-ce --showduplicates | sort -r
sudo dnf install --nobest docker-ce
sudo systemctl start docker

# ServerA box (where internet access is there)
docker login                                                       <br>
docker pull openjdk:8-jre                                          <br>
docker tag openjdk:8-jre erich/openjdk:8-jre                       <br>                
docker save erich/openjdk:8-jre > openjdk8.tar                     <br>  
git clone https://gitlab.com/advocatediablo/jenkins-container.git  <br>
cd && cd jenkins-container && docker build -t jenkinstest .        <br>
docker container run -d -p 8080:8080 -v /var/localjenkins:/root/.jenkins --name jenkinsserver1 jenkinstest <br> <br>

The next steps are for the target server to take the above "existing Jenkins" to the new server.<br>
# ServerB Steps (if not starting on a fresh server)
sudo docker rm -f jenkinsserver1                                  <br>
docker container rm $(docker container ls -aq)                    <br>
cd && rm -Rf jenkins-container/                                   <br>
sudo rm -rf /var/localjenkins                                     <br>
docker rmi $(docker images -a -q) --force                         <br> 
---those are the steps that were used on the ServerB to start over <br>

# Ship from ServerA to ServerB 
docker stop jenkinsserver1                                         <br>
sudo tar -czvf file.tar.gz /var/localjenkins                       <br>
scp -i ~/.ssh/id_san3 *.*tar*  advocatediablo@Ephemeral.IP:/home/advocatediablo <br> 

# ServerB (fresh server - these steps run without www)
git clone https://gitlab.com/advocatediablo/jenkins-container.git  <br>
sudo docker load --input openjdk8.tar                              <br>
cd && cd jenkins-container && sudo docker build -t jenkinstest .   <br>
sudo mkdir /var/localjenkins && sudo tar -xzvf file.tar.gz -C /    <br> 
sudo docker container run -d -p 8080:8080 -v /var/localjenkins:/root/.jenkins --name jenkinsserver1 jenkinstest  <br> 

# Startup Existing Server
sudo systemctl start docker                                        <br>

# Calling Remote jobs
1) turn on the API Token in your user profile. <br>
2) add fields f1 / f2 (for the below example)  <br>
curl -X POST -u admin:GOODLUCKTOKEN-LOL-1386927a  'http://x.x.x.x:8080/job/test/buildWithParameters?f1=tst&f2=xyz'; <br>
echo $?

